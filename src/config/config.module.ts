import { Module } from '@nestjs/common';
import { ApiModule } from 'api/api.module';
import { DatabaseModule } from 'config/database/database.module';

@Module({
  imports: [DatabaseModule, ApiModule],
  controllers: [],
  providers: [],
})
export class ConfigModule {}
