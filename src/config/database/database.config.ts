import { dbConfig } from '../../constants/database';

const { DB_HOST, DB_PORT, DB_NAME } = dbConfig;

export default {
  uri: `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, // Replace with your MongoDB connection URI
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
};
