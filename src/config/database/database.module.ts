import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import databaseConfig from './database.config';

@Module({
  imports: [
    MongooseModule.forRoot(
      databaseConfig.uri,
      databaseConfig.options as MongooseModuleOptions,
    ),
  ],
  exports: [MongooseModule],
})
export class DatabaseModule {}
