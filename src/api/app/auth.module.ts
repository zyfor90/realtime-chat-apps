import { Module } from '@nestjs/common';
import { AppController } from '../app/auth.controller';
import { AppService } from '../app/auth.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AuthModule {}
