import { Module } from '@nestjs/common';
import { AuthModule } from 'api/app/auth.module';

@Module({
  imports: [AuthModule],
  controllers: [],
  providers: [],
})
export class ApiModule {}
