import { NestFactory } from '@nestjs/core';
import { ConfigModule } from './config/config.module';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(ConfigModule);
  app.use(helmet());
  await app.listen(3000);
}
bootstrap();
