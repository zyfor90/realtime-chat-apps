import 'dotenv/config';

// * Define type data here

interface DatabaseConfig {
  readonly DB_HOST: string | undefined;
  readonly DB_NAME: string | undefined;
  readonly DB_PORT: string | undefined;
}

// Define value and object here

export const dbConfig: DatabaseConfig = {
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_NAME: process.env.DB_NAME,
};
