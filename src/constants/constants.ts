// * Define type data here
interface ApplicationConfig {
  readonly ErrUndefined: undefined;
}

// Define value and object here
export const constants: ApplicationConfig = {
  ErrUndefined: undefined,
};
